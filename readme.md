# Acquired 3DS library

Services to complete the client side steps of Acquired's 3DS v2 process.  The server side pieces
are clearly beyond the scope of this library.

Details can be found at http://developer.acquired.com/3ds/3dsv2

## Creating new version

1. Make code changes
1. Manually update `version` inside `package.json`
1. Commit your changes
1. Tag the commit in GIT under the same version number
1. Git push
1. `npm publish`.  This will automatically run the `prepare` command in `package.json`, which will 
build the distribution version.