import { BrowserDataInterface } from './browser-data.interface';

/**
 * Collects mundane information about the customer's browser.  This is needs to be submitted
 * with 3DS v2 transactions.
 */
export class BrowserDataService {
  public static gatherBrowserData(): BrowserDataInterface {
    const dt = new Date();

    // Apparently square brackets are not allowed in their system.
    let userAgent = navigator.userAgent;
    userAgent = userAgent.replace(/\[|\]/g, '');

    return {
      browserLanguage: navigator.language,
      browserTime: dt,
      browserTimezoneZoneOffset: dt.getTimezoneOffset() / 60,
      colorDepth: screen.colorDepth,
      javaEnabled: navigator.javaEnabled(),
      screenHeight: screen.height,
      screenWidth: screen.width,
      userAgent,
    };
  }
}
