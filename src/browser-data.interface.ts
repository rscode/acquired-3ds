/**
 * Structure of data returned by BrowserDataService
 */
export interface BrowserDataInterface {
  colorDepth: number;
  javaEnabled: boolean;
  browserLanguage: string;
  screenHeight: number;
  screenWidth: number;
  userAgent: string;
  browserTime: object;
  browserTimezoneZoneOffset: number;
}
