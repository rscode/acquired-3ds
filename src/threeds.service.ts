/**
 * @see http://developer.acquired.com/3ds/3dsv2#3dsversion
 */
export class ThreedsService {
  protected readonly IFRAME_NAME = 'acquired-3ds-iframe';

  protected iframeElement: HTMLIFrameElement | null = null;
  protected formElement: HTMLFormElement | null = null;

  createAndSubmit(methodUrl: string, threeDSMethodData: string): void {
    if (this.iframeElement != null) {
      throw Error('Elements already exist.  Must remove before trying again.');
    }

    // Hidden iframe
    this.iframeElement = document.createElement('iframe');
    this.iframeElement.id = this.IFRAME_NAME;
    this.iframeElement.name = this.IFRAME_NAME;
    this.iframeElement.style.display = 'none';
    document.body.appendChild(this.iframeElement);

    // Form to post to iframe
    this.formElement = document.createElement('form');
    this.formElement.setAttribute('method', 'POST');
    this.formElement.setAttribute('action', methodUrl);
    this.formElement.setAttribute('target', this.IFRAME_NAME);

    const methodDataInput = document.createElement('input');
    methodDataInput.setAttribute('type', 'hidden');
    methodDataInput.setAttribute('name', 'threeDSMethodData');
    methodDataInput.setAttribute('value', threeDSMethodData);
    this.formElement.appendChild(methodDataInput);
    document.body.appendChild(this.formElement);

    this.formElement.submit();
  }

  /**
   * Checks if the iframe element exists on the page.  This would indicate that createAndSubmit()
   * was run, and removeElements() was not.
   */
  iframeExists(): boolean {
    return this.iframeElement != null;
  }

  /**
   * Removes elements created by this service from the DOM.
   */
  removeElements(): void {
    this.formElement?.remove();
    this.iframeElement?.remove();

    this.iframeElement = null;
    this.formElement = null;
  }
}
